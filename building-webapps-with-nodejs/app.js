var http = require('http');
var logger = require('./logger');
var Point = require('./Point')

http.createServer(function(request, response) {
  logger.info('responding to a request');
  var point = new Point(2, 3);
  response.writeHead({
    'Content-Type':'text/plain'
  });
  
  response.end('Hello Point: ' + point.toString());
  logger.info('done responding to request');
}).listen(3000);