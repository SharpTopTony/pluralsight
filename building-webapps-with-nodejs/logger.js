exports.info = function(msg) {
  console.log(infoPrefix() + msg);
};

exports.error = function(msg) {
  console.error(errorPrefix() + msg);
};

function infoPrefix() {
  return 'INFO: ' + now() + ' -- ';
}

function errorPrefix() {
  return 'ERROR: ' + now() + ' -- ';
}

function now() { 
  return new Date() 
}