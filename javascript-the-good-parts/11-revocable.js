function add(a,b) {
  return a + b;
}

function mul(a, b) {
  return a * b;
}

function addf(x) {
	return function (y) {
		return add(x, y);
	}
}

function applyf(f) {
  return function (x) {
	  return function(y) {
		  return f(x, y);
	  };
  };
}

function curry(func, x) {
	return function (y) {
		return func(x, y);
	};
}

function methodize (func) {
	return function(y) {
		return func(this, y);
  };
}

function demethodize (meth) {
  return function(x, y) {
    return x.meth(y);
  };
}

function twice(func) {
  return function (n) {
    return func(n, n);
  };
}

double = twice(add);
square = twice(mul);

function composeUnary(f, g) {
  return function(n) {
    return g(f(n));
  };
}

function composeBinary(f, g) {
  return function(x, y, z) {
    return g(f(x, y), z);
  };
}

function once(func) {
  var hasRun = false;
  return function() {
    if (hasRun) { 
      throw 'This method can only be called once.';
    }
    hasRun = true;
    return func.apply(this, arguments);
  };
}

function counterFactory(n) {
  return {
    inc: function() {
      n += 1;
      return n;
    },
    dec: function() {
      n -= 1;
      return n;
    },
    print: function() {
      console.log(n);
    }
  };
}

function revocable(func) {
  var isRevoked = false;
  var that = this;
  return {
    revoke: function() { isRevoked = true },
    invoke: function() {
      if (isRevoked) {
        throw 'This method has been revoked';
      }
      return func.apply(that, arguments);
    }
  };
}

function revocable2(func) {
  var that = this;
  return {
    revoke: function() { func = null },
    invoke: function() { return func.apply(that, arguments); }
  };
}

revocableAlert = revocable2(alert);
revocableAlert.invoke(7);
revocableAlert.revoke();
revocableAlert.invoke(8);
