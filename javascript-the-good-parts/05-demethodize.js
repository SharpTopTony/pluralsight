function add(a,b) {
  return a + b;
}

function mul(a, b) {
  return a * b;
}

function addf(x) {
	return function (y) {
		return add(x,y);
	}
}

function applyf(f) {
  return function (x) {
	  return function(y) {
		  return f(x,y);
	  };
  };
}

function curry(func, x) {
	return function (y) {
		return func(x,y);
	};
}

function methodize (func) {
	return function(y) {
		return func(this,y);
  };
}

function demethodize (meth) {
  return function(x, y) {
    return x.meth(y);
  };
}

Number.prototype.add = methodize(add);

demethAdd = demethodize(Number.prototype.add);
console.log(demethAdd(4,5));