function add(a,b) {
  return a + b;
}

function mul(a, b) {
  return a * b;
}

function addf(x) {
	return function (y) {
		return add(x,y);
	}
}

function applyf(f) {
  return function (x) {
	  return function(y) {
		  return f(x,y);
	  };
  };
}

function curry(func, x) {
	return function (y) {
		return func(x,y);
	};
}

function methodize (func) {
	return function(y) {
		return func(this,y);
  };
}

Number.prototype.add = methodize(add);
console.log((3).add(4));