var num = 0;

function identity(x) {
  return x;
}

function identityf(x) {
  return function() {
    return x;
  };
}

function add(a,b) {
  return a + b;
}

function addf(x) {
  return function(y) {
    return x + y;
  }
}

function mul(a, b) {
  return a * b;
}

var idf = identityf(3);

num = addf(3)(4);
console.log(num);