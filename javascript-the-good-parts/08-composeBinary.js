function add(a,b) {
  return a + b;
}

function mul(a, b) {
  return a * b;
}

function addf(x) {
	return function (y) {
		return add(x, y);
	}
}

function applyf(f) {
  return function (x) {
	  return function(y) {
		  return f(x, y);
	  };
  };
}

function curry(func, x) {
	return function (y) {
		return func(x, y);
	};
}

function methodize (func) {
	return function(y) {
		return func(this, y);
  };
}

function demethodize (meth) {
  return function(x, y) {
    return x.meth(y);
  };
}

function twice(func) {
  return function (n) {
    return func(n, n);
  };
}

double = twice(add);
square = twice(mul);

function composeUnary(f, g) {
  return function(n) {
    return g(f(n));
  };
}

function composeBinary(f, g) {
  return function(x, y, z) {
    return g(f(x, y), z);
  };
}

console.log(composeBinary(add, mul)(2, 3, 4));