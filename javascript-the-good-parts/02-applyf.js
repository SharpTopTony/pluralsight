function add(a,b) {
  return a + b;
}

function mul(a, b) {
  return a * b;
}

function applyf(f) {
  return function (x) {
	  return function(y) {
		  return f(x,y);
	  };
  };
}

addf = applyf(add)
num = addf(3)(4);
console.log(num);
console.log(applyf(mul)(3)(4));