function add(a,b) {
  return a + b;
}

function mul(a, b) {
  return a * b;
}

function curry(func, x) {
  return function (y) {
  	  return func(x,y);
	};
}

add3 = curry(add, 3)
console.log(add3(5));
console.log(curry(mul, 3)(5));