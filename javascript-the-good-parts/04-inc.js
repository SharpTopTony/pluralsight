function add(a,b) {
  return a + b;
}

function mul(a, b) {
  return a * b;
}

function addf(x) {
	return function (y) {
		return add(x,y);
	}
}

function applyf(f) {
  return function (x) {
	  return function(y) {
		  return f(x,y);
	  };
  };
}

function curry(func, x) {
	return function (y) {
		return func(x,y);
	};
}

incadd = addf(1);

incaf = applyf(add)(1);

inccurry = curry(add, 1);

console.log(incadd(7));
console.log(incaf(7));
console.log(inccurry(7));
